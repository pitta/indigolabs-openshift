#!/usr/bin/env bash
echo "Bootstrap and configure httpd instance."

#Validate the Discovery layer is available and ready - Retry 12 times wait 5 seconds between tries
    source /etc/bootstrap/bin/testReady.sh discovery 12 5

#TODO - The tomcat user file will not take place until Tomcat restart
echo "Creating HTTP config from template"
    consul-template -consul consul:8500 -once -template "/etc/httpd/templates/httpd.ctmpl:/etc/httpd/conf/httpd.conf"
echo "Creating Private HTML page from template"
    consul-template -consul consul:8500 -once -template "/etc/httpd/templates/private-index.ctmpl:/var/www/html/private/index.html"
