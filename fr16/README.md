# FR16 Docker Stack configuration files, scripts and templates #

## Stack Features ##

* Complete ForgeRock stack
* Multiple integration point between all the components

## docker-compose.yml details ##

* Define dependencies of the ForgeRock stack to ensure the containers are started in the right order
* Assign Project prefixed Docker image name to ensure image integrity accross multiple projects
* Assign Project prefixed Docker container to allow container to co-exists
* Creates a SoftWareDefineNetwork to connect all the containers
* Assigns aliases with Domain to allow FQDN to be used accross the stack
* Creates and assign volumes to OpenAM, OpenDJ and Consul (TODO - Add OpenIDM?) to allow data persistence
* Startup K/V store used for discovery and configuration
* Injects the required environement variables into the bootstrap container - providing a single configuration entry point for the whole stack

## Discovery container configuration ##

* We use Hashicorp's Consul as the stack distributed K/V store.
* We run the container as-is with docker-compose and use the command "agent -dev -ui -client 0.0.0.0"
* All containers will use the K/V store to create configurations from templates.
