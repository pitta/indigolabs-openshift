#!/usr/bin/env bash

#Display Consul Server FQDN used
    echo "Consul Server: consul"

#---TEST LOOP---#
#Make sure Discovery layer had time to properly instantiate and go through leader election
#Loop 5 times for a total of 30 seconds
#Different test for election leader vs containers looking for discovery ready flag
    echo ""
    ((count = 5))
    while [[ $count -ne 0 ]] ; do
        ready=`curl --silent http://consul:8500/v1/kv/?recurse`;
        if [[ $ready == "No cluster leader" ]];
        	then
        		echo "consul:8500 not ready - counter: $count";
        		sleep 6;
        	else
        		echo "Successfully connected to consul:8500";
        		((count = 1));
        fi
        ((count = count - 1));
    done
    unset count;
    unset ready;
#---TEST LOOP---#

#Validate if Key/Values are already loaded and K/V is ready
    bootstrapped=`curl --silent http://consul:8500/v1/kv/platform/ready/discovery|jq '.[0].Value' --raw-output|base64 --decode`;

    if [ "$bootstrapped" = true ] ;
        then
            echo 'Already Bootstrapped!'
        else
            echo 'Bootstrapping started'
            #Bootstraping value passed by environment variables
                echo ""
                echo "BootStraping variables passed to container"
                echo "Domain: $DOMAIN"
                echo "BaseDN: $BASE_DN"
                echo "Master Password: $MASTER_PASSWORD"
            #Set the keyvalue in Consul to enable servers to bootstrap themselves
                echo ""
                echo "Domain Value set"
                curl --silent --request PUT --data $DOMAIN http://consul:8500/v1/kv/platform/global/domain
                echo ""
                echo "Base DN Value set"
                curl --silent --request PUT --data $BASE_DN http://consul:8500/v1/kv/platform/global/basedn
                echo ""
                echo "Master Password Value set"
                curl --silent --request PUT --data $MASTER_PASSWORD http://consul:8500/v1/kv/platform/global/masterpassword
            #Set Discovery ready flag to indicate K/V are loaded and containers can start consuming
                echo ""
                echo "Finished bootstrapping platform - set value to true"
                curl --silent --request PUT --data "true" http://consul:8500/v1/kv/platform/ready/discovery
    fi

#Display the stored value from the keystore
    echo ""
    echo "Keys in Consul Key/Value Store after bootstrap"
    echo "Domain: `curl --silent http://consul:8500/v1/kv/platform/global/domain|jq '.[0].Value' --raw-output|base64 --decode`"
    echo "BaseDN: `curl --silent http://consul:8500/v1/kv/platform/global/basedn|jq '.[0].Value' --raw-output|base64 --decode`"
    echo "Master Password: `curl --silent http://consul:8500/v1/kv/platform/global/masterpassword|jq '.[0].Value' --raw-output|base64 --decode`"
    echo "Discovery Ready: `curl --silent http://consul:8500/v1/kv/platform/ready/discovery|jq '.[0].Value' --raw-output|base64 --decode`"

#Validate the Directory is up and running - Retry 30 Times wait 12 seconds between tries
    source /etc/bootstrap/bin/testDirectory.sh $MASTER_PASSWORD $BASE_DN 30 12
    if [[ $directoryready == "1" ]] ;
        then
            #Set the flag to true in the K/V
            curl --silent --request PUT --data "true" http://consul:8500/v1/kv/platform/ready/opendj
        else
            echo 'DJ was not started properly'
    fi
    unset directoryready

#Display value directly from K/V
    echo ""
    echo "OpenDJ Ready: `curl --silent http://consul:8500/v1/kv/platform/ready/opendj|jq '.[0].Value' --raw-output|base64 --decode`"

#Validate OpenAM is up and running - Retry 30 Times wait 12 seconds between tries
  source /etc/bootstrap/bin/testOpenam.sh $DOMAIN 30 12
  if [[ $openamstatus == "200" ]] ;
      then
          #Set the flag to true in the K/V
          curl --silent --request PUT --data "true" http://consul:8500/v1/kv/platform/ready/openam
      else
          echo 'OpenAM was not started properly'
  fi
  unset openamstatus

#Display value directly from K/V
  echo ""
  echo "OpenAM Ready: `curl --silent http://consul:8500/v1/kv/platform/ready/openam|jq '.[0].Value' --raw-output|base64 --decode`"

#Validate OpenIDM is up and running - Retry 30 Times wait 12 seconds between tries
  source /etc/bootstrap/bin/testOpenidm.sh $DOMAIN 30 12
  if [[ $openidmstatus == "ACTIVE_READY" ]] ;
      then
          #Set the flag to true in the K/V
          curl --silent --request PUT --data "true" http://consul:8500/v1/kv/platform/ready/openidm
      else
          echo 'OpenIDM was not started properly'
  fi
  unset openidmstatus

#Display value directly from K/V
  echo ""
  echo "OpenIDM Ready: `curl --silent http://consul:8500/v1/kv/platform/ready/openidm|jq '.[0].Value' --raw-output|base64 --decode`"

#Done Bootsraping and validating components status
  echo ""
  echo "BootStrap completed - container will exit now"
