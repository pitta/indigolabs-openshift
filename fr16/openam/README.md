# OpenAM #

## Container Special consideration ##

* When used with docker-compose.yml file - /opt/openam/ is mounted on a persistent volume that will survive container restart
* If container is restarted and configuration is found - Bootstrapping will not occur leading to some configuration files and ampass not to be found in container
* To restart from scratch - delete with docker volume or with docker-compose down -v

## Features Overview ##

* Basic configuration against external OpenDJ
* Create /internal realm and configure Authentication/DataStore to use OpenDJ ou=internal branch
* Provide policy evaluation entitlement to the LDAP group Policy_Admins
* Create SAML IDP and SP to provide Federation with OpenIG acting as the SP
* Create Resource Type, PolicySet and Policy under /internal realm to provide PEP integration with OpenIG

## Important files and locations in the container ##

* Validate Discovery and Directory is ready: /etc/bootstrap/bin/testReady.sh
* Container configuration script: /etc/openam/bin/configure.sh
* OpenAM amadmin password used for ssoadm: /etc/openam/secrets/ampass
* OpenAM setup configuration templates: /etc/openam/templates/
* OpenAM setup configuration files: /etc/openam/conf/
* OpenAM running configuration files: /opt/openam/
