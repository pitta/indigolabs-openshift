#!/usr/bin/env bash
echo "OpenAM Configuration not found. Bootstrap and configuring instance."

#Validate the Discovery layer is available and ready - Retry 12 times wait 5 seconds between tries
    source /etc/bootstrap/bin/testReady.sh discovery 12 5

#Validate OpenDJ is available and ready - Retry 30 times wait 12 seconds between tries
    source /etc/bootstrap/bin/testReady.sh opendj 30 12

#Create Tomcat specific configuration BEFORE starting it to avoid restart
    consul-template -consul consul:8500 -once -template "/etc/openam/templates/tomcat-users.ctmpl:/opt/tomcat/conf/tomcat-users.xml"
    /opt/tomcat/bin/startup.sh

#Create files from templates
    consul-template -consul consul:8500 -once -template "/etc/openam/templates/ampass.ctmpl:/etc/openam/secrets/ampass:chmod 400 /etc/openam/secrets/ampass"
    consul-template -consul consul:8500 -once -template "/etc/openam/templates/OpenAM-Base.ctmpl:/etc/openam/conf/OpenAM-Base.conf"
    consul-template -consul consul:8500 -once -template "/etc/openam/templates/OpenDJ-AuthModule.ctmpl:/etc/openam/conf/OpenDJ-AuthModule.conf"
    consul-template -consul consul:8500 -once -template "/etc/openam/templates/OpenDJ-DataStore.ctmpl:/etc/openam/conf/OpenDJ-DataStore.conf"
    consul-template -consul consul:8500 -once -template "/etc/openam/templates/SSOADM-Realm-Auth.ctmpl:/etc/openam/conf/SSOADM-Realm-Auth.conf"

#Validate Tomcat is up - We should land on the OpenAM wizard config page.
    /bin/wget --spider --tries 20 --retry-connrefused --no-check-certificate http://openam:8080/openam

#Configure OpenAM with template
    java -jar /opt/forgerock/openam/configurator/openam-configurator-tool-13.0.0.jar --file /etc/openam/conf/OpenAM-Base.conf

#Deploy SSOADM tools
    cd /opt/forgerock/openam/ssoadm/
    ./setup --acceptLicense --path /opt/openam/

#Execute batch of SSOADM commands to create basic configuration - Internal Realm, LDAP Auth Module, Auth Chain, DataStore
    /opt/forgerock/openam/ssoadm/openam/bin/ssoadm do-batch --adminid amadmin --password-file /etc/openam/secrets/ampass -Z /etc/openam/conf/SSOADM-Realm-Auth.conf --batchstatus /tmp/realm-auth.ssoadm.log --continue

#TODO - Federation Could be created conditionnally based on K/V Flag
#Create SAML configuration from templates
    consul-template -consul consul:8500 -once -template "/etc/openam/templates/SAML-OpenAM-idp.ctmpl:/etc/openam/conf/SAML-OpenAM-idp.xml"
    consul-template -consul consul:8500 -once -template "/etc/openam/templates/SAML-OpenAM-idp-ext.ctmpl:/etc/openam/conf/SAML-OpenAM-idp-ext.xml"
    consul-template -consul consul:8500 -once -template "/etc/openam/templates/SAML-OpenIG-sp.ctmpl:/etc/openam/conf/SAML-OpenIG-sp.xml"
    consul-template -consul consul:8500 -once -template "/etc/openam/templates/SAML-OpenIG-sp-ext.ctmpl:/etc/openam/conf/SAML-OpenIG-sp-ext.xml"

#Execute batch of SSOADM commands to create SAML IDP and SPs configurations
    /opt/forgerock/openam/ssoadm/openam/bin/ssoadm do-batch --adminid amadmin --password-file /etc/openam/secrets/ampass -Z /etc/openam/conf/SSOADM-Federation.conf --batchstatus /tmp/federation.ssoadm.log --continue

#Create OpenID Connect Agent configuration from templates
    consul-template -consul consul:8500 -once -template "/etc/openam/templates/OIDC-OpenIGAgent.ctmpl:/etc/openam/conf/OIDC-OpenIGAgent.conf"

#Execute batch of SSOADM commands to create OpenID Connect configuration
     /opt/forgerock/openam/ssoadm/openam/bin/ssoadm do-batch --adminid amadmin --password-file /etc/openam/secrets/ampass -Z /etc/openam/conf/SSOADM-OIDC.conf --batchstatus /tmp/oidc.ssoadm.log --continue

#Create Policy creation script from template - on creation make executable
    consul-template -consul consul:8500 -once -template "/etc/openam/templates/REST-CreatePolicies.ctmpl:/etc/openam/bin/REST-CreatePolicies.sh:chmod +x /etc/openam/bin/REST-CreatePolicies.sh"

#Execute REST API policy creation script
    exec /etc/openam/bin/REST-CreatePolicies.sh
