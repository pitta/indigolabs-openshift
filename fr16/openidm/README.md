# OpenIDM #

## Features Overview ##

* LDAP Connector to OpenDJ
* CSV Connector to see IDM/DJ

## Important files and locations in the container ##

* Validate Discovery and Directory is ready: /etc/bootstrap/bin/testReady.sh
* Configure the OpenIDM instance: /etc/openidm/bin/configure.sh
* OpenIDM configuration templates: /etc/openidm/templates/
* Openidm running configuration: /opt/openidm/
