# OpenDJ container #

## Container Special consideration ##

* When used with docker-compose.yml file - /opt/opendj/ is mounted on a persistent volume that will survive container restart
* If container is restarted and configuration is found - Bootstrapping will not occur leading to some configuration files and dm-pw not to be found in container
* To restart from scratch - delete with docker volume or with docker-compose down -v

## Features Overview ##

* Single OpenDJ to hold Configuration, CTS, and Datastore
* Complete DIT with best practices ACIs
* OpenAM Schema
* OpenAM indexes
* Test User created in internal
* Load Balancer account for health checks and validations
* OpenAM Policy evaluation account and group

## Important files and locations in the container ##

* Validate Discovery is ready: /etc/bootstrap/bin/testReady.sh
* Container configuration script: /etc/opendj/bin/configure.sh
* OpenDJ Directory Manager password used for condifuration: /etc/opendj/secrets/dm-pw
* OpenDJ setup configuration templates: /etc/opendj/templates/
* OpenDJ LDIFs file: /etc/opendj/ldif/
* OpenDJ setup configuration files: /etc/opendj/conf/
* OpenDJ running configuration files: /opt/opendj/
