# ForgeRock Docker Base Images #

* Source files for the base images.
* Built directly on DockerHub.
* Can be overwritten and built locally with docker-compose build.
* Allows pulling latest images from DockerHub easily with docker-compose pull.