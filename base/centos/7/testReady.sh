#!/usr/bin/env bash

# Call this script to make sure a component is up and running
# Call with the number of retries as well as sleep time (seconds).
# sh scriptname 1 2 3
    if [ $# -ne 3 ]
    then
      echo "Usage: testReady.sh <component> <retries> <waittime>"
      exit $E_BADARGS
    fi

#Map arguments to variables
  component=$1
  retries=$2
  waittime=$3

#Parameters passed:
    echo "Readiness loop will execute $retries times with a sleep time of $waittime for a total of: $(($retries * $waittime)) seconds"

#Make sure the component had time to startup, configure itself and be marked as ready
    ((count = $2))
    while [[ $count -ne 0 ]] ; do
        ready=`curl --silent http://consul:8500/v1/kv/platform/ready/$component|jq '.[0].Value' --raw-output|base64 --decode`;
        if [[ $ready != "true" ]];
        	then
        		echo "$component not ready - counter: $count - time remaining: $(($count * $waittime)) seconds";
        		sleep $waittime;
        	else
        		echo "Successfully connected to $component";
        		((count = 1));
        fi
        ((count = count - 1));
    done

#CleanUp variables
    unset count
    unset ready
    unset component
    unset retries
    unset waittime
